# Build image (https://aboullaite.me/multi-stage-docker-java/)
FROM maven:3.6.3-jdk-8 as build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn --batch-mode -f /usr/src/app/pom.xml clean package

# Production image
FROM openjdk:8-jdk
EXPOSE 8080
COPY --from=build /usr/src/app/target/*.jar /usr/app/
WORKDIR /usr/app

CMD ["/bin/bash", "-c", "find -type f -name '*.jar' | xargs java -jar"]
